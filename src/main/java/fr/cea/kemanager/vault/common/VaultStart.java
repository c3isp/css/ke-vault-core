package fr.cea.kemanager.vault.common;

import java.net.URI;
import java.util.Map;

import org.springframework.vault.authentication.TokenAuthentication;
import org.springframework.vault.client.VaultEndpoint;
import org.springframework.vault.core.VaultSysTemplate;
import org.springframework.vault.core.VaultTemplate;
import org.springframework.vault.core.VaultTransitTemplate;
import org.springframework.vault.support.VaultMount;

/**
 * This class encapsulates the operations for the connection to the Vault server.
 *
 * @author Quentin Lutz
 */
public class VaultStart {

	public VaultStart(String token) {
		super();
		this.token = token;
	}


	/**
	 * Create a new {@link VaultTransitTemplate} from a token.
	 *
	 */
	private String token;

	public VaultTransitTemplate GetTransit() {
			
		
		VaultTemplate vaultTemplate = GetTemplate();

		VaultSysTemplate system = new VaultSysTemplate(vaultTemplate);

		
		
		Map<String,VaultMount> mounts = system.getMounts();
		if (!mounts.containsKey("transit/")) {
			system.mount("transit", VaultMount.create("transit"));
		}


		VaultTransitTemplate transit = (VaultTransitTemplate) vaultTemplate.opsForTransit("transit");
		return transit;

	}


	public VaultTemplate GetTemplate() {
		return new VaultTemplate(VaultEndpoint.from(URI.create("http://127.0.0.1:8200")),new TokenAuthentication(token));
	}

}
